<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome1', 'AuthController@welcome1');

Route::get('/test1', function(){
    return view('adminlte.master');
});

Route::get('/index', function(){
    return view('items.index');
});

Route::get('/data-tables', function(){
    return view('items.datatables');
});

// Route::get('/pertanyaan/create', 'PertanyaanController@create');
// Route::post('/pertanyaan','PertanyaanController@store'); //@store untuk menyimpan data
// Route::get('/pertanyaan','PertanyaanController@index')->name('post.index');
// Route::get('/pertanyaan/{pertanyaan_id}','PertanyaanController@show');
// Route::get('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController@edit');
// Route::put('/pertanyaan/{pertanyaan_id}', 'PertanyaanController@update');
// Route::delete('/pertanyaan/{pertanyaan_id}','PertanyaanController@destroy');


Route::resource('pertanyaan','PertanyaanController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
