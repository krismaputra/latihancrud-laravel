<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    //
    protected $table = "pertanyaan";
    //jika menggunakan mass assignmen
    protected $fillable = ["judul", "isi"];
    //jika menggunak guarded menentukan column mana yang tidak boleh digunakan atau di insert
    // protected $guarded = [];

    public function author(){
        return $this->belongsTo('App\User','id_user');
    }
}
