<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
use Auth;

class PertanyaanController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }
    //
    public function create(){
        return view('pertanyaan.create');
    }

    public function store(Request $request){
       //dd($request->all()); //untuk mengecek data apakah sudah masuk dan seperti apa
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required'
        ]);

        // ini merupakan query builder
    //    $query = DB::table('pertanyaan')->insert([ 
    //         "judul" => $request["judul"],
    //         "isi" => $request["isi"]
    //    ]);
        // ini meenggunakan ORM
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan-> save();

        //menggunakan mass asignment pilih kolom mana yang boleh di isi secara mass assignmen
        $pertanyaan = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            // "profil_id" => Auth::id()
        ]);
        
        $user = Auth::user();
        $user -> pertanyaan() ->save($pertanyaan);

        // $user->save();
       return redirect('/pertanyaan')->with('success','Pertanyaan Berhasil Disimpan!');

    }

    public function index(){
        //menggunakan query builder
        // $pertanyaan = DB::table('pertanyaan')->get();
        // dd($pertanyaan);
        //menggunakan model assignmen
        $user = Auth::user();
        $pertanyaan = $user->pertanyaan; 
        // $pertanyaan = Pertanyaan::all(); //::all(); memanggil semua di database
        return view('pertanyaan.index', compact('pertanyaan')); //compact untuk menjadikan array
    }

    public function show($pertanyaan_id){
        // $post = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        // dd($post);

        $post = Pertanyaan::find($pertanyaan_id);
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($pertanyaan_id){
        // $post = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        $post = Pertanyaan::find($pertanyaan_id);
        return view('pertanyaan.edit', compact('post'));
    }

    public function update($pertanyaan_id, Request $request){
        $request->validate([
            'judul' => 'required',
            'isi' => 'required'
        ]);

        // $query = DB::table('pertanyaan')
        //             ->where('id', $pertanyaan_id)
        //             ->update([
        //                 'judul' => $request['judul'],
        //                 'isi' => $request['isi']
        //             ]); 
        
        $update = Pertanyaan::where('id', $pertanyaan_id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success', 'Berhasil update pertanyaan');
    }

    public function destroy($pertanyaan_id){
        // $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();

        Pertanyaan::destroy($pertanyaan_id);
        return redirect('/pertanyaan')->with('success', 'Berhasil di hapus');
    }
}
