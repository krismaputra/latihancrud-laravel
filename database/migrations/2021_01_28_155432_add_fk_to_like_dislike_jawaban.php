<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToLikeDislikeJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('like_dislike_jawaban', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('profil_id');
            $table->unsignedBigInteger('jawaban1_id');
            $table->foreign('jawaban1_id')->references('id')->on('jawaban');
            $table->foreign('profil_id')->references('id')->on('profile');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('like_dislike_jawaban', function (Blueprint $table) {
            //
            $table->dropForeign(['jawaban1_id']);
            $table->dropForeign(['profil_id1']);
            $table->dropColumn(['jawaban1_id']);
            $table->dropColumn(['profil_id']);

        });
    }
}
