<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFkToLikeDislikePertanyaan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('like_dislike_pertanyaan', function (Blueprint $table) {
            $table->unsignedBigInteger('pertanyaan1_id');
            $table->unsignedBigInteger('profil_id');
            $table->foreign('pertanyaan1_id')->references('id')->on('pertanyaan');
            $table->foreign('profil_id')->references('id')->on('profile');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('like_dislike_pertanyaan', function (Blueprint $table) {
            //
            $table->dropForeign(['pertanyaan1_id']);
            $table->dropForeign(['profil_id']);
            $table->dropColumn(['pertanyaan1_id']);
            $table->dropColumn(['profil_id']);

        });
    }
}
