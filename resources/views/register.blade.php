<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome1" method="POST">
    @csrf
        <label for="fname">First Name : </label><br><br>
        <input type="text" name="fname" id="fname"><br><br>
        <label for="lname">Last Name : </label><br><br>
        <input type="text" name="lname" id="lname"><br><br>
        <label for="">Gender : </label> <br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other <br><br>
        <label>Nationality :</label><br><br>
        <select name="nationality" id="">
            <option value="indonesian">indonesian</option>
            <option value="malaysian">malaysian</option>
            <option value="australian">australian</option>
            <option value="other">other</option>
        </select><br><br>
        <label>Langue Spoken : </label><br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>
        <label>Bio : </label> <br><br>
        <textarea name="biodata" cols="30" rows="10"></textarea><br><br>
        <input type="submit">

    </form>
</body>

</html>