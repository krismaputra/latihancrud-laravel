@extends('adminlte.master')

@section('content')
<div class="container ml-3 mt-3">
<div class=" mt-3 ml-3">
    <h4>{{$post->judul}}</h4>
    <p>{{$post->isi}}</p>
    <p> Author : {{$post->author->name}}</p>  
</div>

<a href="/pertanyaan" class="btn btn-primary btn-sm">Home</a>
</div>
@endsection